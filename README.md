### What is this repository for? ###

* This is a updated and commented version of the RouteMe2 app, it mainly consists of a BLE beacon scanner, GPS, fingerprinting and sensor fusion algorithm, and state machine with audio q's to notify the user

### How do I get set up? ###
To run this, you must have an android device with BLE, GPS, and Location capabilities....
First download all files into android studios.
build the app on your device
If the app crashes, you may have to manually go into system preferences to give the app location privileges 

### Who do I talk to? ###

Anthony Chong
anthonymchong@gmail.com